# Requirements
* A Computer
* Python

# How to Prepare the Environment
```
git clone https://git.dende.de/dende/untappd-plotter.git
cd untappd-plotter
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```


# How to use

* log in to untappd.com
* go to chrome dev tools
* go to network tab
* go to the main request to untappd.com
* go to the headers tab
* copy the content of the cookies header to 'cookie.txt'
* execute download.py