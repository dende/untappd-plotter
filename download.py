import json
import time
from pprint import pprint

import requests
import yaml
from lxml import etree
from io import StringIO
import dateparser
from peewee import *
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)

usernames = ["Dendemeier", "8lour", "Harryak", "standpipelol", "Beasty42"]
db = SqliteDatabase('beer.db')


class User(Model):
    name = CharField(unique=True)
    checkins = []
    url = 'https://untappd.com/user/{$user}/beers'
    more_url = 'https://untappd.com/profile/more_beer/{$user}/{$checkin_number}?sort=date'
    last_checkin = None

    def add_checkin(self, checkin):
        self.checkins.append(checkin)

    def get_last_checkin(self):
        return self.last_checkin

    def get_url(self):
        return self.url.replace('{$user}', self.name)

    def get_more_url(self, checkin_number):
        return self.more_url.replace('{$user}', self.name).replace('{$checkin_number}', str(checkin_number))

    class Meta:
        database = db


class Checkin(Model):
    user = ForeignKeyField(User, backref='checkins')
    date = DateTimeField()
    id = IntegerField(unique=True)

    class Meta:
        database = db


class UntappdStatsDownloader:
    user = None
    cookies = None

    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'cache-control': 'no-cache',
        'pragma': 'no-cache',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
    }

    def __init__(self, user, cookies):
        logger.info("Creating downloader for %s", user)
        self.user, created = User.get_or_create(name=user)
        if created:
            logger.info("Created new user %s", user)
        else:
            logger.info("User %s already existed", user)
        self.cookies = cookies

    def collect_checkins(self, items):
        created = 0
        for item in items:
            datelink = item.xpath(".//a[@class='track-click' and @data-href=':firstCheckin']")[0]
            checkin_url = datelink.get('href').strip('/')
            checkin_url_parts = checkin_url.split('/')
            checkin_id = checkin_url_parts[-1]
            datestring = "".join(datelink.itertext())
            date = dateparser.parse(datestring)
            try:
                with db.atomic():
                    Checkin.create(user=self.user, date=date, id=checkin_id)
                    self.user.last_checkin = checkin_id
                    created = created + 1
            except IntegrityError:
                logger.error("Checkin already exists, aborting")
                break
        return created

    def initial_download(self):
        r = requests.get(self.user.get_url(), headers=self.headers, cookies=self.cookies)
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(r.text), parser)
        try:
            items = tree.xpath("//div[@class='beer-item']")
            return self.collect_checkins(items)
        except AssertionError:
            logger.error("Did not find any item")
            return 0

    def download_more(self, checkin_number):
        url = self.user.get_more_url(checkin_number)
        headers = self.headers
        headers['referer'] = self.user.get_url()
        headers['x-requested-with'] = 'XMLHttpRequest'
        r = requests.get(url, headers=self.headers, cookies=self.cookies)
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(r.text), parser)
        try:
            items = tree.xpath("//body/div[@class='beer-item']")
            return self.collect_checkins(items)
        except AssertionError:
            logger.error("Did not find any more items")
            return 0

    def run(self):
        downloaded = self.initial_download()
        checkin_number = downloaded
        while downloaded > 0:
            downloaded = self.download_more(checkin_number)
            checkin_number = checkin_number + downloaded
            logger.info("Downloaded %s more checkins", downloaded)
            time.sleep(2)
        logger.info("Downloaded %s checkins for %s", checkin_number, self.user.name)


def get_login_cookies(data):
    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'cache-control': 'no-cache',
        'pragma': 'no-cache',
        'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0'
    }
    login_url = "https://untappd.com/login"
    with requests.Session() as s:
        s.headers.update(headers)
        r = s.get(login_url)
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(r.text), parser)
        try:
            session_key_input = tree.xpath("//input[@id='session_key']")[0]
            session_key = session_key_input.get('value')
            logger.info("Got an session key: %s", session_key)
        except AssertionError as err:
            logger.error("Did not find any item")
            return 0
        data["session_key"] = session_key

        r = s.post(login_url, data=data)
        if s.cookies.get('untappd_user_v3_e') is None:
            logger.critical("No cookies found")
        else:
            logger.info("Got an user Cookie: %s", s.cookies.get('untappd_user_v3_e'))
        return s.cookies


def parse_cookie_string(cookieString):
    cookies = {}
    cookieStringParts = cookieString.split("; ")
    for cookie in cookieStringParts:
        parts = cookie.split("=")
        key = parts[0]
        if not key.startswith('_'):
            value = parts[1]
            cookies[key] = value
    return cookies


def main():
    cookies = None
    try:
        with open("cookie.txt", "r") as f:
            cookie_string = f.read().replace('\n', '')
            cookies = parse_cookie_string(cookie_string)
    except:
        logger.error("cookie.txt not found")

    if cookies == None:
        credentials = None
        try:
            with open("credentials.yaml", "r") as f:
                credentials = yaml.safe_load(f)
        except:
            logger.error("credentials.yaml not found")

        if credentials != None:
            cookies = get_login_cookies(credentials)

    db.connect()
    db.create_tables([User, Checkin])

    for user in usernames:
        downloader = UntappdStatsDownloader(user, cookies)
        downloader.run()

    data = {}
    users = User.select()
    for user in users:
        data[user.name] = []
        for checkin in Checkin.select().where(Checkin.user == user.id).order_by(Checkin.id.asc()):
            data[user.name].append(checkin.date)

    with open('beer.json', 'w+') as f:
        json.dump(data, f)


if __name__ == "__main__":
    main()
